<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'product', 'middleware' => 'auth'], function () {
    Route::get('/new', ['uses' => 'HomeController@create', 'as' => 'product.create']);
    Route::post('/', ['uses' => 'HomeController@store', 'as' => 'product.store']);
    Route::get('/{id}', ['uses' => 'HomeController@show', 'as' => 'product.show']);
    Route::put('/{id}', ['uses' => 'HomeController@update', 'as' => 'product.update']);
    Route::delete('/{id}', ['uses' => 'HomeController@destroy', 'as' => 'product.destroy']);
});
