<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = [
        'name', 'price', 'discount', 'discounted_price', 'collection', 'RecID', 'avatar', 'userId', 'publish',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, "userId");
    }
}
