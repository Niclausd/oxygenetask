<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DropdownList extends Model
{
    protected $fillable = [
        'code', 'name', 'type', 'developer', 'category',
    ];
    public function user()
    {
        return $this->belongsTo(User::class, "developer");
    }
}
