<?php

namespace App\Http\Controllers;

use App\Models\DropdownList;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    protected function validateData()
    {
        $data = request()->validate([
            'name' => 'required',
            'price' => 'required',
            'discount' => '',
            'discounted_price' => '',
            'collection' => '',
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'userId' => 'required',
            'publish' => 'required',
        ]);
        return $data;
    }

    public function Index()
    {
        try {
            $user = Auth::user();
            $products = Products::where('userId', $user->id)->orderByDesc('id')->get();

            request()->session()->flash("loading", "Details Loaded successfully");
            return view('home', [
                'user' => $user,
                'products' => $products,
            ]);

        } catch (\Exception $exp) {
            request()->session()->flash('error', 'An error has occured ' . $exp->getMessage());
        }
    }

    public function create()
    {
        try {
            $user = auth()->user();
            $colle = DropdownList::where("type", "Collection")->get();
            $publish = DropdownList::where("type", "YesNo")->get();
            $product = new Products();

            return view('products.create', [
                'user' => $user,
                'colle' => $colle,
                'publish' => $publish,
                'product' => $product,
            ]);
        } catch (\Exception $exp) {
            return redirect()->back()->with('error', 'An error has occured ' . $exp->getMessage());
        }
    }

    public function store(Request $req)
    {

        $dt = $this->validateData();
        try {
            $product = Products::create($dt);
            $RecID = sprintf('PRD%06d', $product->id);
            $product->update(['RecID' => $RecID]);

            $imageName = $RecID . '.' . $dt['avatar']->extension();
            $req->avatar->storeAs('/product/', $imageName);
            Image::make($req->file('avatar'))->resize(300, 350)->save(public_path('storage/product/' . $imageName));
            $aPath = '/storage/product/' . $imageName;
            $product->update(['avatar' => $aPath]);

            return redirect('/product/' . $product->id)->with("success", "Details saved successfully");
        } catch (\Exception $exp) {
            dd($exp->getMessage());
            return redirect()->back()->with('error', 'An error has occured ' . $exp->getMessage());
        }
    }

    public function show(Products $id)
    {
        try {
            $user = auth()->user();
            $colle = DropdownList::where("type", "Collection")->get();
            $publish = DropdownList::where("type", "YesNo")->get();

            return view('products.create', [
                'user' => $user,
                'colle' => $colle,
                'publish' => $publish,
                'product' => $id,
            ]);

        } catch (\Exception $exp) {
            return redirect()->back()->with('error', 'An error has occured ' . $exp->getMessage());
        }
    }

    public function update(Products $id, Request $req)
    {
        $dt = $this->validateData();

        $imageName = $id->RecID . '.' . $dt['avatar']->extension();
        $dt['avatar']->storeAs('/product/', $imageName);
        Image::make($req->file('avatar'))->resize(300, 350)->save(public_path('storage/product/' . $imageName));
        $aPath = '/storage/product/' . $imageName;
        $dt['avatar'] = $aPath;

        try {
            $id->update($dt);
            return redirect()->back()->with("success", "Details updated successfully");
        } catch (\Exception $exp) {
            return redirect()->back()->with('error', 'An error has occured ' . $exp->getMessage());
        }
    }

}
