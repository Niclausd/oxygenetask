<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductsResource;
use App\Models\Products;

class ProductsController extends Controller
{
    public function Index()
    {
        try {
            $prop = Products::with('user')->where('publish', 'Yes')
                ->orderBy('id')->get();

            return response()->json([
                'resp' => 'products loaded Successfully',
                'body' => ProductsResource::collection($prop),
                'status' => 1,
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'resp' => $ex->getMessage(),
                'status' => 0,
                'body' => [],
            ]);
        }
    }

    public function Show(Request $req)
    {
        try {
            $prop = Products::with('user')->where('publish', 'Yes')
                ->where('collection', $req->coll)->orderBy('id')->get();
            return response()->json([
                'resp' => 'products loaded Successfully',
                'body' => ProductsResource::collection($prop),
                'status' => 1,
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'resp' => $ex->getMessage(),
                'status' => 0,
                'body' => [],
            ]);
        }
    }

}
