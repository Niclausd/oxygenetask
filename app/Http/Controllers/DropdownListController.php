<?php

namespace App\Http\Controllers;

use App\Models\Models\DropdownList;
use Illuminate\Http\Request;

class DropdownListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Models\DropdownList  $dropdownList
     * @return \Illuminate\Http\Response
     */
    public function show(DropdownList $dropdownList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Models\DropdownList  $dropdownList
     * @return \Illuminate\Http\Response
     */
    public function edit(DropdownList $dropdownList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Models\DropdownList  $dropdownList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DropdownList $dropdownList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Models\DropdownList  $dropdownList
     * @return \Illuminate\Http\Response
     */
    public function destroy(DropdownList $dropdownList)
    {
        //
    }
}
