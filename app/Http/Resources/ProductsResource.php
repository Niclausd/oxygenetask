<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'price' => $this->price,
            'discount' => $this->discount,
            'discounted_price' => $this->discounted_price,
            'collection' => $this->collection,
            'RecID' => $this->RecID,
            'avatar' => $this->avatar,
            'userId' => $this->userId,
            'publish' => $this->publish,
            'user_email' => $this->User->email,
            'user_name' => $this->User->name,
            'created_at' => (string) $this->created_at,
        ];
    }
}
