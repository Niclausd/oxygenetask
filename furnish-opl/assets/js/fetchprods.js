const api_path = "http://localhost:8000/api/products";

async function getapi(url) {
  const response = await fetch(url);
  var data = await response.json();
  console.log(data.body);
  show(data.body);
}

getapi(api_path);

// Function to define innerHTML for HTML table
function show(data) {
  for (let r of data) {
    if (r.collection == "Furniture") {
      var col = document.createElement("div");
      col.className = "col-md-4";
      var el = document.createElement("div");
      el.className = "single-product-items";
      el.insertAdjacentHTML(
        "beforeend",
        `<div class="product-item-image"><a href="#">
    <img src="http://localhost:8000${r.avatar}"alt="Product"></a><div class="product-discount-tag">
    <p>-${r.discount}%</p></div></div><div class="product-item-content text-center mt-30">
    <h5 class="product-title"><a href="#">${r.name}</a></h5>
    <ul class="rating">
    <li><i class="lni-star-filled"></i></li>
    <li><i class="lni-star-filled"></i></li>
    <li><i class="lni-star-filled"></i></li>
    </ul>
    <span class="regular-price">${r.discounted_price}</span>
    <span class="discount-price">${r.price}</span>
    </div>`
      );
      col.appendChild(el);
      document.getElementById("furniture").appendChild(col);
    } else if (r.collection == "Decorative") {
      var col = document.createElement("div");
      col.className = "col-md-4";
      var el = document.createElement("div");
      el.className = "single-product-items";
      el.insertAdjacentHTML(
        "beforeend",
        `<div class="product-item-image"><a href="#">
    <img src="http://localhost:8000${r.avatar}"alt="Product"></a><div class="product-discount-tag">
    <p>-${r.discount}%</p></div></div><div class="product-item-content text-center mt-30">
    <h5 class="product-title"><a href="#">${r.name}</a></h5>
    <ul class="rating">
    <li><i class="lni-star-filled"></i></li>
    <li><i class="lni-star-filled"></i></li>
    <li><i class="lni-star-filled"></i></li>
    </ul>
    <span class="regular-price">${r.discounted_price}</span>
    <span class="discount-price">${r.price}</span>
    </div>`
      );
      col.appendChild(el);
      document.getElementById("Decorative").appendChild(col);
    } else if (r.collection == "Lighting") {
      var col = document.createElement("div");
      col.className = "col-md-4";
      var el = document.createElement("div");
      el.className = "single-product-items";
      el.insertAdjacentHTML(
        "beforeend",
        `<div class="product-item-image"><a href="#">
    <img src="http://localhost:8000${r.avatar}"alt="Product"></a><div class="product-discount-tag">
    <p>-${r.discount}%</p></div></div><div class="product-item-content text-center mt-30">
    <h5 class="product-title"><a href="#">${r.name}</a></h5>
    <ul class="rating">
    <li><i class="lni-star-filled"></i></li>
    <li><i class="lni-star-filled"></i></li>
    <li><i class="lni-star-filled"></i></li>
    </ul>
    <span class="regular-price">${r.discounted_price}</span>
    <span class="discount-price">${r.price}</span>
    </div>`
      );
      col.appendChild(el);
      document.getElementById("Lighting").appendChild(col);
    } else if (r.collection == "OutDoor") {
      var col = document.createElement("div");
      col.className = "col-md-4";
      var el = document.createElement("div");
      el.className = "single-product-items";
      el.insertAdjacentHTML(
        "beforeend",
        `<div class="product-item-image"><a href="#">
    <img src="http://localhost:8000${r.avatar}"alt="Product"></a><div class="product-discount-tag">
    <p>-${r.discount}%</p></div></div><div class="product-item-content text-center mt-30">
    <h5 class="product-title"><a href="#">${r.name}</a></h5>
    <ul class="rating">
    <li><i class="lni-star-filled"></i></li>
    <li><i class="lni-star-filled"></i></li>
    <li><i class="lni-star-filled"></i></li>
    </ul>
    <span class="regular-price">${r.discounted_price}</span>
    <span class="discount-price">${r.price}</span>
    </div>`
      );
      col.appendChild(el);
      document.getElementById("OutDoor").appendChild(col);
    } else if (r.collection == "Storage") {
      var col = document.createElement("div");
      col.className = "col-md-4";
      var el = document.createElement("div");
      el.className = "single-product-items";
      el.insertAdjacentHTML(
        "beforeend",
        `<div class="product-item-image"><a href="#">
    <img src="http://localhost:8000${r.avatar}"alt="Product"></a><div class="product-discount-tag">
    <p>-${r.discount}%</p></div></div><div class="product-item-content text-center mt-30">
    <h5 class="product-title"><a href="#">${r.name}</a></h5>
    <ul class="rating">
    <li><i class="lni-star-filled"></i></li>
    <li><i class="lni-star-filled"></i></li>
    <li><i class="lni-star-filled"></i></li>
    </ul>
    <span class="regular-price">${r.discounted_price}</span>
    <span class="discount-price">${r.price}</span>
    </div>`
      );
      col.appendChild(el);
      document.getElementById("Storage").appendChild(col);
    }
  }
}
