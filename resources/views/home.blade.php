@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <a class="btn btn-outline-success btn-sm m-10" href="{{route('product.create')}}"><i class="fa fa-plus"></i>
                Add New Product</a>
        </div>
        <div class="col-md-6">
            <h3 class=" text-primary">Products Section</h3>
        </div>
        @forelse ($products as $prd)
        <div class="col-md-4">
            <div class="card">
                <img src="{{ asset($prd->avatar) }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">{{$prd->name}}</h5>
                    <p class="card-text">{{$prd->collection}}</p>
                    <a href="{{route('product.show',$prd->id)}}" class="btn btn-primary btn-sm"><i
                            class="fa fa-eye"></i>
                        Edit/View</a>
                </div>
                <div class="card-footer"><small class="text-muted">Kshs {{$prd->price}} with
                        <strong>{{$prd->discount}}</strong>%
                        discount</small></div>
            </div>
        </div>
        @empty
        <h4>No Products Found</h4>
        @endforelse
    </div>
</div>
@endsection