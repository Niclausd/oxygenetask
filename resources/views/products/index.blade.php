@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-6">
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop">
        <i class=" fa fa-plus"></i> Add New Product
      </button>
    </div>
    <div class="col-md-6">
      <h3 class=" text-primary">Products Section</h3>
    </div>
    <div class="col-md-4">
      <div class="card" style="width: 18rem;">
        <img src="..." class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">Chair</h5>
          <p class="card-text">Furniture.</p>
        </div>
        <div class="card-footer"><small class="text-muted">Kshs 200 with 20% discount</small></div>
      </div>
    </div>
  </div>
</div>
@endsection