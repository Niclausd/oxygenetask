@extends('layouts.app')

@section('content')
<div class="container">

  <div class="row">

    <div class="col-md-12">
      {!! Form::open(['route'=>'product.store','method'=>'post', 'files' => true]) !!}
      @if($product->id != null)
      @method('PUT')
      @endif
      {{-- {{Form::token()}} --}}
      <div class="row">
        <div class="col-md-12">
          {{ Form::label('name', 'Name') }}
          {{ Form::text('name', $product->name??'', ['class' => 'form-control']) }}
          {{Form::hidden('userId',$user->id)}}
          <p class="text-danger"><i>@error('name') {{$message}} @enderror</i></p>
        </div>
        <div class="col-md-6">
          {{ Form::label('price', 'Price') }}
          {{ Form::number('price', $product->price??'', ['class' => 'form-control']) }}
          <p class="text-danger"><i>@error('price') {{$message}} @enderror</i></p>
        </div>
        <div class="col-md-6">
          {{ Form::label('discount', 'Discount') }}
          {{ Form::number('discount', $product->discount??'', ['class' => 'form-control']) }}
          <p class="text-danger"><i>@error('discount') {{$message}} @enderror</i></p>
        </div>
        <div class="col-md-6">
          {{ Form::label('discounted_price', 'Discounted Price') }}
          {{ Form::number('discounted_price', $product->discounted_price??'', ['class' => 'form-control']) }}
          <p class="text-danger"><i>@error('discounted_price') {{$message}} @enderror</i></p>
        </div>
        <div class="col-md-6">
          <label for="collection">Collection</label>
          <select class="form-control" name="collection">
            <option value="" selected>Select a collection</option>
            @foreach($colle as $bld)
            <option value='{{ $bld->code }}' {{ ( $bld->code == $product->collection) ? 'selected' : '' }}>
              {{ $bld->name }}
            </option>
            @endforeach
          </select>
          <p class="text-danger"><i>@error('collection') {{$message}} @enderror</i></p>
        </div>
        <div class="col-md-6">
          {{ Form::label('publish', 'publish') }}
          <select class="form-control" name="publish">
            <option value="" selected>Publish?</option>
            @foreach($publish as $bld)
            <option value='{{ $bld->code }}' {{ ( $bld->code == $product->publish) ? 'selected' : '' }}>
              {{ $bld->name }}
            </option>
            @endforeach
          </select>
          <p class="text-danger"><i>@error('publish') {{$message}} @enderror</i></p>
        </div>
        <div class="col-md-12">
          {{ Form::label('avatar', 'Image') }}
          <input type="file" name="avatar" />
          <p class="text-danger"><i>@error('avatar') {{$message}} @enderror</i></p>
        </div>
        <div class="row">
          <div class="col-md-12">
            @if($product->id == null)
            <button type="submit" class="btn btn-primary mt-1"><i class="fa fa-save"></i> Save
              details
            </button>
            @else
            <button type="submit" class="btn btn-info mt-1"
              onclick="javascript: form.action='/product/{{$product->id}}';"><i class="fa fa-edit"></i> Save details
            </button>
            @endif
            <a href="/home" class="btn btn-danger mt-1"><i class="fa fa-close text-white"></i>
              Close</a>
          </div>
        </div>
        {!! Form::close() !!}
      </div>
    </div>

  </div>
  @endsection